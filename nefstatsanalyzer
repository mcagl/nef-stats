#!/usr/bin/env python3

import argparse
import os
import pandas as pd
import seaborn as sns
import sys

from matplotlib import pyplot as plt


def export_html_report():
    """ Export a nice html containing the graphs.
    We create the file and group all the resources in the parent folder of the
    analyzed csv.
    """
    pass


def get_output_folder(csvfile):
    """ Return the folder where to save files to.
    """
    output_folder = os.path.join(os.path.dirname(csvfile), 'analyzed')

    if os.path.exists(output_folder):
        print(f"Output directory ({output_folder}) already exists")
        reply = input(
            "\tContinue and override the contents? (y/n) "
        )
        if reply == 'y':
            pass
        else:
            # TODO: switch to exception asap
            return 1

    if not os.path.exists(output_folder):
        os.mkdir(output_folder)

    return output_folder


def start_analysis(csvfile):
    """ The main entrypoint for the analysis of the csv.
    """
    output_folder = get_output_folder(csvfile)

    df = pd.read_csv(csvfile)
    sns.lmplot(x='Focal length', y='ISO', data=df, fit_reg=False)
    plt.savefig(
        os.path.join(output_folder, 'focal-length_iso.png'),
        dpi=300
    )

    plt.figure(figsize=(16, 6))
    sns.distplot(df['Focal length'], kde=False, bins=40)
    plt.savefig(
        os.path.join(output_folder, 'focal-length_hist.png'),
        dpi=300
    )

    plt.figure(figsize=(16, 6))
    sns.stripplot(
        x="Shutter speed",
        y="ISO",
        data=df,
        jitter=True,
        hue="Aperture"
    )
    plt.savefig(
        os.path.join(output_folder, 'shutter-speed_iso_aperture_strip.png'),
        dpi=300
    )


def main():
    parser = argparse.ArgumentParser(
        description="Analyze data exported with nef-stats"
    )
    parser.add_argument("input", help="The csv file to read", type=str)

    args = parser.parse_args()
    if not os.path.exists(args.input):
        print(f"Input file {args.input} doesn't exists")
        return 1

    dataset_file = args.input
    start_analysis(dataset_file)


if __name__ == "__main__":
    sys.exit(main())
