# -*- coding: utf-8 -*-

import csv
import os
import unittest

# We use this trick because the python file has a dash in it
exec(open('./nef-stats').read())


COLUMNS = ['Filename', 'Focal length', 'Aperture', 'Shutter speed', 'ISO']


class TestCsv(unittest.TestCase):

    def setUp(self):
        self.directory = "."
        self.output = "./tests/test.csv"

        main_csv_export(outfile=self.output, dir=self.directory)

    def test_csv_exists(self):
        self.assertTrue(os.path.isfile("./tests/test.csv"))

    def test_csv_exported_values(self):
        with open(self.output) as csvfile:
            csvreader = csv.reader(csvfile)
            head_row = csvreader.__next__()
            data_row = csvreader.__next__()
            dictionary = dict(zip(head_row, data_row))
            self.assertEqual(dictionary['Focal length'], '50')
            self.assertEqual(dictionary['Aperture'], 'f/5.6')
            self.assertEqual(dictionary['Shutter speed'], '1/80')
            self.assertEqual(dictionary['ISO'], '320')

    def test_csv_columns(self):
        with open(self.output) as csvfile:
            csvreader = csv.reader(csvfile)
            head_row = csvreader.__next__()
            for title in COLUMNS:
                self.assertIn(
                    title,
                    head_row,
                    f"{title} not in expected columns"
                )

    def tearDown(self):
        os.remove(self.output)


if __name__ == '__main__':
    unittest.main()
